#!/bin/bash
#   Title:          installCraftsman.py
#   Author:         Matthew Wycliff
#   Date:           8.13.2021
#   Update:         8.18.2021

#   Prerequisites:  python3, git, 
#                   You must be able to access
#                   traclabs bitbucket and github
#
#   Prerequisites:  Install CRAFTSMAN and its acommpanying
#                   packages / libraries.


import sys
import os
from os import getcwd, system, chdir, popen, environ
import subprocess
import time

sleeptime_1 = 0.1
cwd = ""
cwd = os.getcwd()
bash_tag=""


def welcomeMsg():
    time.sleep(sleeptime_1)
    welcome_msg = "WELCOME TO INSTALL CRAFTSMAN"
    print("\n\n" + welcome_msg + "\n\n")
    time.sleep(sleeptime_1)
    welcome_msg = "You will be asked to enter your password.\
    \nBe ready to enter your credentials when prompted."
    print("\n\n" + welcome_msg + "\n\n")
    time.sleep(sleeptime_1)


def removeLocks():
    system("sudo rm /var/lib/dpkg/lock*")


def installRequiredPackages():
    system("sudo apt install " + "python3-wstool" + " -y")
    system("sudo apt install " + "build-essential" + " -y")
    system("sudo apt install " + "python3-clint" + " -y")
    system("sudo apt install " + "python3-pip" + " -y")
    system("sudo apt install " + "python3-rosdep" + " -y")
    system("sudo apt install " + "python3-rosdep2" + " -y")
    system("sudo apt install " + "python3-rosinstall" + " -y")
    system("sudo apt install " + "python3-rosinstall-generator" + " -y")
    system("sudo apt install " + "python3-rosdistro-modules" + " -y")
    system("sudo apt install " + "ros-noetic-catkin"  + " -y")


def checkRepoCreds(): # Check and enforce code repository credentials
    time.sleep(sleeptime_1)
    print("\n *** " + "CHECK GITHUB CREDENTIALS" + " ***\n")
    print("\n *** " + "CHECK BITBUCKET CREDENTIALS" + " ***\n")
    print("\n *** " + "ENFORCE REPOSITORY CREDENTIALS" + " ***\n")


def installCurl():
    time.sleep(sleeptime_1)
    print("\n *** " + "INSTALL CURL" + " ***\n")
    os.system("sudo apt install -y curl")


def setupRosSources():
    time.sleep(sleeptime_1)
    print("\n *** " + "SETUP ROS SOURCES" + " ***\n")
    os.system("sudo sh -c \
    'echo \"deb http://packages.ros.org/ros/ubuntu \
    $(lsb_release -sc) main\" > \
    /etc/apt/sources.list.d/ros-latest.list'")


def setupRosKeys():
    time.sleep(sleeptime_1)
    print("\n *** " + "SETUP ROS KEYS" + " ***\n")
    os.system("curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -")


def sudoAptUpdate():
    time.sleep(sleeptime_1)
    print("\n *** " + "SUDO APT UPDATE" + " ***\n")
    os.system("sudo apt update")


def installRos():
    time.sleep(sleeptime_1)
    print("\n *** " + "INSTALL ROS" + " ***\n")
    os.system("sudo apt install ros-noetic-desktop-full -y")


def sourceNoeticSetupBash():
    time.sleep(sleeptime_1)
    print("\n *** " + "SOURCE OPT/ROS/NOETIC/SETUP.BASH" + " ***\n")
    os.system("source /opt/ros/noetic/setup.bash")


def appendBashrc(tag):
    time.sleep(sleeptime_1)
    print("\n *** " + "APPEND BASH" + " ***\n")
    system("sh -c \'printf \""
        + "\n\n"
        + tag
        + "\n\n"
        + "\" >> ~/.bashrc\'")
    time.sleep(sleeptime_1)
    system("tail ~/.bashrc")


def rosdepInitUpdate():
    print("\n *** " + "ROSDEP INIT" + " ***\n")
    time.sleep(sleeptime_1)
    system("sudo " + "rosdep init")
    time.sleep(sleeptime_1)
    print("\n *** " + "ROSDEP UPDATE" + " ***\n")
    system("sudo " + "rosdep update")


def cloneCraftsman(d):
    path = d + "/craftsman" 
    craftsman_present = os.path.isdir(path)
    print(craftsman_present)
    if(not craftsman_present):
        print("\n *** " + "CLONE CRAFTSMAN" + " ***\n")
        os.system("git clone git@bitbucket.org:traclabs/craftsman_dev.git " + d + "/craftsman")
    else:
        print("\n *** " + "PASS" + " ***\n")
        pass
    print(str(d + "/craftsman"))
    os.system("cd " + d + "/craftsman;\
    ls -1;\
    catkin init;\
    catkin config;\
    git checkout devel;")


def pythonPip():
    time.sleep(sleeptime_1)
    system("sudo pip3 install -U catkin_tools")
    system("sudo pip3 install rosdep")


def installPyCraftsman():
    time.sleep(sleeptime_1)
    print("\n *** " + "INSTALL.PY" + " ***\n")
    time.sleep(sleeptime_1)
    os.system("./install.py")
    os.system("./install.py -v RSP")


def checkoutAdamant():
    time.sleep(sleeptime_1)
    print("\n *** " + "CHECKOUT ADAMANT" + " ***\n")
    os.system("cd " + cwd + "/src/trac_ik;git checkout adamant;")
    

def rosdepInstallFromPaths():
    time.sleep(sleeptime_1)
    print("\n *** " + "ROSDEP INSTALL FROM PATHS" + " ***\n")
    os.system("rosdep install --from-paths . --ignore-src -y -r;"\
    + "sudo apt install libnlopt-dev ros-noetic-gazebo-ros-control"\
    + "ros-noetic-joint-state-controller;")


def installRobots():
    print("\n *** " + "INSTALL ROBOTS" + " ***\n")
    time.sleep(sleeptime_1)
    os.system("./install.py -v ROBOTS")


def extraPackageInstaller():
    print("\n *** " + "EXTRA PACKAGE INSTALLER" + " ***\n")
    time.sleep(sleeptime_1)
    system("sudo apt install " + "ros-noetic-fcl" + " -y")
    system("sudo apt install " + "ros-noetic-costmap-2d" + " -y")
    system("sudo apt install " + "ros-noetic-moveit-core" + " -y")
    system("sudo apt install " + "ros-noetic-libfranka" + " -y")
    system("sudo apt install " + "ros-noetic-moveit-ros-planning" + " -y")
    system("sudo apt install " + "ros-noetic-moveit-ros-planning-interface" + " -y")


def catkinBuild():
    time.sleep(sleeptime_1)
    system("catkin build")


if __name__ == "__main__":
    if(not os.path.isdir(cwd + '/src')):
        welcomeMsg()
        removeLocks()
        sudoAptUpdate()
        removeLocks()
        checkRepoCreds()
        installCurl()
        removeLocks()
        setupRosSources()
        setupRosKeys()
        sudoAptUpdate()
        removeLocks()
        installRequiredPackages()
        removeLocks()
        installRos()
        sourceNoeticSetupBash()
        bash_tag="source /opt/ros/noetic/setup.bash"
        appendBashrc(bash_tag)
        rosdepInitUpdate()
        bash_tag="catkin() {\n\
        if [[ \$1 == \\\"config\\\" ]]; then\n\
            command catkin config -DCMAKE_BUILD_TYPE=Release --extend /opt/ros/\$ROS_DISTRO\n\
        else\n\
            command catkin \\\"\$@\\\"\n\
        fi\n}"
        appendBashrc(bash_tag)
        bash_tag="cd " + cwd + "/craftsman/"
        appendBashrc(bash_tag)
        bash_tag="alias craftsman=\\\"cd " + cwd\
        + "/craftsman;source devel/setup.bash; rospack profile;\\\""
        appendBashrc(bash_tag)
        cloneCraftsman(cwd)
        print("\n\n\nOpen a new terminal window and enter this command:\n\
        $ python3 ../installCraftsman.py")
        exit(0)
    else:
        if(not os.path.isfile(cwd + '/VERSION_INFO.txt')):
            print("\nYou are in the wrong directory.\n\
            Open a new terminal window, and make sure you\n\
            are in the YOUR_PATH/craftsman/ directory before proceeding.\n\
            \n$ python3 ../installCraftsman.py")
        else:            
            dpath = cwd + '/src/craftsman_application_tools'
            c1 = os.path.isdir(dpath)
            dpath = cwd + '/src/pal_sigslot'
            c2 = os.path.isdir(dpath)
            if(not c1 and not c2):
                print("\nINSTALL.PY\n")
                sudoAptUpdate()
                pythonPip()
                installPyCraftsman()
                sudoAptUpdate()
                installRobots()
                extraPackageInstaller()
                checkoutAdamant()
                rosdepInstallFromPaths()
                print("\n\n\nOpen a new terminal window and enter these commands:\n\n"\
                + "\t$ craftsman\n\n"\
                + "\t$ source /opt/ros/noetic/setup.bash\n\n"\
                + "\t$ python3 ../installCraftsman.py")
                exit(0)
            else:
                print("\n\n\nDid you remember to issue these commands\n"
                + "before running installCraftsman.py?:\n\n"\
                + "$ craftsman\n"\
                + "$ source /opt/ros/noetic/setup.bash")
                val = input("\n\n\ny/n?\n\n\n")
                if(val=='y'):
                    print("\nProceed.")
                else:
                    print("\nIssue those commands and then try again.")
                    exit(0)
                print("\n*** BUILDING CRAFTSMAN WITH CATKIN TOOL ***\n")
                catkinBuild()
                print("\nDONE COMPILING CRAFTSMAN!\n")
                print("\nYou are finished!\n\
                    Open a fresh terminal and enter this command: '$ craftsman'\n\
                    Then enter:\n\t$ roslaunch craftsman_demos panda_demo.launch\n")
                bash_tag="source devel/setup.bash"
                appendBashrc(bash_tag)
                exit(0)
