## CRAFTSMAN INSTALLATION SCRIPT
#### DATE                    AUG 16 2021
## *** WARNING: Experimental state ***

### PURPOSE ###

Streamline the CRAFTSMAN installation process.

### SYSTEM REQUIREMENTS ###

You must be using Ubuntu 20.04 Focal Fossa
in order for this python script to work properly.
This will not work on other versions of Linux
including Ubuntu 20.10 Groovy Gorilla and Ubuntu 18.04 Bionic Beaver.

You also must have git installed.
You will need access to TRACLabs bitbucket
and TRACLabs github.

If you lack access either of these repositories
the script will not work.

An easy way to check whether or not you have been granted
access to TRACLabs github is as follows:

    $ git clone https://github.com/traclabs/toppra.git

If you are unable to pull down toppra then you must first
gain access TRACLabs github repositories.
Do this before installing CRAFTSMAN.

# INSTRUCTIONS ###

### 1.     Create a workspace for craftsman.


    $ mkdir ~/craftsman_workspace && cd ~/craftsman_workspace


### 2.     Clone the craftsman_installation_script repository.

    $ git clone git@bitbucket.org:traclabs/craftsman_installation_script.git


    $ cd craftsman_installation_script


### 3.     Run the script using python 3.

    $ python3 installCraftsman.py


### 4.     Follow the instructions that are displayed in the terminal.
You will be prompted for your password at the beginning of
the installation process. You will also be asked to enter
'yes' at various times throughout the procedure.


### 5.      Once the installation process is complete you may run any of the craftsman robot demonstrations.

        $ roslaunch craftsman_robots jaco_demo.launch
                    Or
        $ roslaunch craftsman_robots panda_demo.launch
                    Or
        $ roslaunch craftsman_robots baxter_demo.launch
                    Or
        $ roslaunch craftsman_robots r2.launch
